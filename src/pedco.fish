#! /usr/bin/fish
# Copyright 2021 cnngimenez

# Author: cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function pedco_login

curl 'https://pedco.uncoma.edu.ar/login/index.php' -b run/cookiejar -c run/cookiejar > run/login1.html

function get_login_token
    grep 'logintoken' run/login1.html | gawk '/.*/ {match($0, /value="(.*)"/, ret); print ret[1];}'
end

set logintoken (get_login_token)

if test -z "$username"
    echo "Username?"
    read username
end
if test -z "$password"
    echo "Password?"
    read -s password
end

curl 'https://pedco.uncoma.edu.ar/login/index.php' -X POST -b run/cookiejar -c run/cookiejar --data-urlencode 'anchor=' --data-urlencode "logintoken=$logintoken" --data-urlencode "username=$username" --data-urlencode "password=$password" --data-urlencode "rememberusername=1" > run/login2.html

end

function pedco_get_actividades

if test -z "$month"
    set month (date "+%m" )
end
if test -z "$year"
    set year (date "+%Y")
end
if test -z "$hour"
    set hour 0
end
if test -z "$minute"
    set minute 0
end
if test -z "$sortby"
    set sortby "datedesc"
end

if test -z "$courseid"
    echo "course ID?"
    read courseid 
end

if test -z "$day"
    echo "day?"
    read day
end

echo "Course ID: $courseid
Obteniendo notificaciones desde $day/$month/$year
Ordenar usando método $sortby"

curl "https://pedco.uncoma.edu.ar/course/recent.php?id=$courseid" -b run/cookiejar -c run/cookiejar > run/recent1.html

function get_sessionkey
  cat run/recent1.html | grep "session" | gawk '/./ {match($0,/"sesskey":"([^"]+)",/,ret); print ret[1]}'
end

set sessionkey (get_sessionkey)

curl 'https://pedco.uncoma.edu.ar/course/recent.php' -X POST -b run/cookiejar -c run/cookiejar  --data-urlencode "id=$courseid" --data-urlencode "sesskey=$sessionkey" --data-urlencode '_qf__recent_form=1' --data-urlencode 'mform_showmore_id_filters=0' --data-urlencode 'mform_isexpanded_id_filters=1' --data-urlencode 'user=0' --data-urlencode 'modid=' --data-urlencode 'group=0' --data-urlencode "sortby=$sortby" --data-urlencode "date[day]=$day" --data-urlencode "date[month]=$month" --data-urlencode "date[year]=$year" --data-urlencode "date[hour]=$hour" --data-urlencode "date[minute]=$minute" --data-urlencode 'date[enabled]=1' --data-urlencode 'submitbutton=Mostrar+actividad+reciente' > run/recent2.html

gawk -f src/get_data.awk run/recent2.html > "run/recent-activites-$courseid.html"

end

function pedco_parse_info
    if test -z "$courseid"
        echa "Course ID?"
        read courseid
    end
    gawk -f src/parse_items.awk "run/recent-activities-$courseid.html" | less
end
