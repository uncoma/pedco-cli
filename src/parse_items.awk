BEGIN {
    section = ""
    type = ""
}

/Tarea:/ {
    section = "Tarea"
    match($0, /Tarea: (.*)$/, text)
    print "Section: Tarea"
    print "Section name: " text[1]
    next
}

/title="Foro"/ {
    section = "Foro"
    print "Section: Foro"
    getline
    match($0, />(.*)$/, text)
    print "Section name: " text[1]
    next
}

/div class="title bold">/ {
    type = "title"
    next
}

/<time/ {
    type = "date"
}

/<\/a>.*[[:digit:]][[:digit:]]:[[:digit:]][[:digit:]]$/{
    type = "date"
}

/div class="user">/ {
    type = "user"
    next
}

/<a href=/ {
    if (type != ""){
        match($0, /href="([^"]+)"/, text)        
    }

if (type == "title"){
    print "    url: " text[1]
}

}

/>.+/ {
    # Print entry if the field type was detected
    if (type != ""){
        match($0, />[^[:alnum:]]*(.*)$/, text)
        print "    " type ": " text[1]
        type = ""
    }
}

/<\/table>/ {
    # Fin de elemento
    print "    -----"
}
