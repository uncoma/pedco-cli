# frozen_string_literal: true

# Copyright 2021 Christian Gimenez
#
# pedco.rb
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'rubygems'
require './pedco_connections'
require './pedco_parsers'
require 'time'

# The PEDCo Module
#
# Módulo para conectarse con PEDCo.
module PEDCo
  # Main class
  class PEDCo
    def initialize(username = nil, password = nil)
      @username = username
      @password = password
      @connection = PEDCoConnections::Connection.new @username, @password
    end

    def connect
      @connection.do_login
      nil
    end

    # Get the recent activities of a course.
    #
    # @param course_id String
    # @param date_from Time Consider activities since this date. If not given,
    #     consider since last week.
    def recent_activity(course_id, date_from = nil)
      # 604_800 is 7 days in seconds
      date_from = Time.new - 604_800 if date_from.nil?

      html = @connection.download_activities course_id, date_from
      parser = PEDCoParsers::DescSortParser.new html
      parser.to_recent_activity
    end

    def my_courses
      html = @connection.download_userpage my_userid

      parser = PEDCoParsers::UserParser.new html
      parser.to_courses
    end

    def my_userid
      html = @connection.download_mainpage
      parser = PEDCoParsers::MainPageParser.new html
      parser.user_id
    end

    def forum(forum_id)
      html = @connection.download_forum forum_id
      parser = PEDCoParsers::DiscussionForumParser.new html
      parser.to_forum
    end

    def inspect
      "#<PEDCo @username=#{@username} @password=... @connection=#{@connection}>"
    end
  end

  # An Activity Notification
  #
  # One of the activity notification parsed from the HTML.
  class Activity
    def initialize(title, date, user, url)
      @title = title
      @date = date
      @user = user
      @url = url
    end

    attr_reader :title, :date, :user, :url

    def to_s
      "Activity date: #{@date} title: #{@title} user: #{@user} url: #{@url}\n"
    end
  end

  # A Section Activity
  #
  # A section is a list of Activity instances. It may came from parsing a forum
  # or task data.
  class SectionActivity
    # @param name String The forum name.
    def initialize(name, activities = [])
      @name = name
      @activities = activities
    end

    attr_reader :name, :activities

    def add(activity)
      @activities.push activity
    end

    def to_s
      actstr = @activities.join
      "Section: #{@name}\n#{actstr}\n"
    end
  end

  # All the recent activity from a course
  class RecentActivity
    def initialize(sections = [])
      @sections = sections
    end

    def to_s
      "Recent activity: #{@sections.join}"
    end

    class << self
      def from_html(html_page)
        parser = RecentActivityHTMLParser.new html_page
        RecentActivity.new parser.sections
      end
    end
  end

  # A Course in PEDCo
  class Course
    def initialize(id, name)
      @id = id
      @name = name
    end

    def to_s
      "Course #{@id}: #{@name}"
    end
  end

  # A forum
  class Forum
    def initialize(name, messages = [])
      @name = name
      @messages = messages
    end

    attr_reader :name, :messages

    def to_s
      "Forum #{@name}. Messages:\n#{@messages}"
    end
  end

  # A forum or discussion message.
  class Message
    # @param id String
    # @param user String
    # @param time Time
    # @param contents Nokogiri::XML::Element
    def initialize(id, user, time, contents)
      @id = id
      @user = user
      @time = time
      @contents = contents
    end

    attr_reader :id, :user, :time, :contents

    def to_s
      "Message #{@id} from #{@user} at #{@time}:\n#{@contents.to_str}"
    end
  end
end
