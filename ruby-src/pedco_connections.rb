# frozen_string_literal: true

# Copyright 2021 Christian Gimenez
#
# pedco_connections.rb
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'rubygems'
require 'mechanize'

module PEDCoConnections
  # This is a PEDCo Connection.
  class Connection
    LOGIN_URL = 'https://pedco.uncoma.edu.ar/login/index.php'

    def initialize(username, password)
      @agent = Mechanize.new
      @username = username
      @password = password
    end

    def inspect
      "#<Connection @username=#{@username} @password=... @agent=#{@agent}>"
    end

    attr_reader :agent

    def do_login
      page = @agent.get LOGIN_URL
      login_form = page.form
      login_form.username = @username
      login_form.password = @password
      # login_form.rememberusername = true

      @agent.submit login_form, login_form.buttons.first
    end

    # @param date Time
    #
    # @return Nokogiri::Page
    def download_activities(course_id, date, sortby = 'datedesc')
      url = "https://pedco.uncoma.edu.ar/course/recent.php?id=#{course_id}"
      page = @agent.get url

      form = page.form
      form['date[day]'] = date.day
      form['date[month]'] = date.month
      form['date[year]'] = date.year
      form['date[hour]'] = date.hour
      form['date[minute]'] = date.min
      form['date[enabled]'] = true
      form.id = course_id
      form.sortby = sortby

      @agent.submit form, form.buttons.first      
    end

    def download_userpage(user_id)
      url = "https://pedco.uncoma.edu.ar/user/profile.php?id=#{user_id}&showallcourses=1"
      @agent.get url
    end

    def download_mainpage
      url = 'https://pedco.uncoma.edu.ar/my/'
      @agent.get url
    end

    def download_forum(forum_id)
      url = "https://pedco.uncoma.edu.ar/mod/forum/discuss.php?d=#{forum_id}"
      @agent.get url
    end
  end

end
