# frozen_string_literal: true

# Copyright 2021 Christian Gimenez
#
# recent_activities.rb
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require './pedco'
require 'time'

if ENV['PEDCO_USERNAME'].nil?
  puts 'Username?'
  username = STDIN.gets
else
  username = ENV['PEDCO_USERNAME']
end

if ENV['PEDCO_PASSWORD'].nil?
  puts 'Password?'
  password = STDIN.gets
else
  password = ENV['PEDCO_PASSWORD']
end

default_datefrom = Time.new - 604_800
if ENV['PEDCO_DATEFROM'].nil?
#  puts "From date? (empty means default: #{default_datefrom.xmlschema})"
#  datefrom = STDIN.gets.chomp
  datefrom = ""
else
  datefrom = ENV['PEDCO_DATEFROM']
end

if datefrom.empty?
  datefrom = default_datefrom
else
  datefrom = Time.xmlschema datefrom
end
puts "Retrieving from date #{datefrom.to_s} up to today."

if ARGV.empty?
  course_ids = []
  while id != "\n"
    puts 'Course ids'
    id = STDIN.gets
    course_ids.push id.chomp if id != "\n"
  end
else
  course_ids = ARGV
end

@pedco = PEDCo::PEDCo.new username, password
puts 'Login in...'
@pedco.connect

@activities = []
course_ids.each do |course_id|
  puts "Downloading activities for #{course_id}..."
  @activities.push @pedco.recent_activity course_id, datefrom
end

puts @activities
