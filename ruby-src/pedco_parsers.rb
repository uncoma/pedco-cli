# frozen_string_literal: true

# Copyright 2021 Christian Gimenez
#
# pedco_parsers.rb
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'rubygems'
require 'nokogiri'
require 'time'
require './pedco'

# Parser for PEDCo pages
#
# This parsers search for relevant information inside HTML pages. These pages
# can be loaded from the PEDCo::Connector or downloaded by using curl and opened
# with File and Nokogiri.
#
# The following example shows how to load a file and use Nokogiri to create the
# Page object.
#
# ```ruby
# require 'nokogiri'
# require 'pedco_parsers'
#
# page = Nokogiri::HTML File.open('./recent.html')
# PEDCoParsers::DescSortParser.new page
# ```
#
# This code can be summarised by using the DescSortParser::from_file method:
#
# ```ruby
# require 'pedco_parsers'
#
# dsp = PEDCoParsers::DescSortParser.from_file './recent.html'
# ```
#
# # Implementation details
# Each class implements the following:
#
# 1. Static methods with XPath to locate each data in the HTML.
# 2. Attributes to store these data when instancing the parser.
# 3. a #to_CLASS method, where CLASS is a PEDCo::CLASS instance.
# 4. Al @element/@page attribute to store the Nokogiri::Element instance with
#    the HTML parsed element. Useful to apply the XPath anytime.
#
# The initialisation of each class creates an attribute which data is provided
# by a class method. For instance, the ActivityParser class defines the
# @username attribute, which is initialised by using ActivityParser::username
# class method.
module PEDCoParsers
  # Parser for an HTML element that contains the activity data.
  class ActivityParser
    def initialize(element)
      @element = element

      @username = ActivityParser.username @element
      @date = Time.xmlschema ActivityParser.date @element
      @title = ActivityParser.title @element
      @url = ActivityParser.url @element
    end

    # A String.
    attr_reader :username

    # A Nokogiri::Element instance.
    attr_reader :element

    # A DateTime instance.
    attr_reader :date

    # A String
    attr_reader :title

    # Avoid displaying @element data which is a pain to the eyes...
    def inspect
      "#<ActivityParser @username=#{@username} @date=#{@date} @title=#{@title}>"
    end

    # Create a PEDCo instance with the parsed data.
    #
    # @return PEDCo::Activity
    def to_activity
      PEDCo::Activity.new @title, @date, @username, @url
    end

    class << self
      # Parse the forum name from the activity HTML.
      #
      # Forum names are stored inside the activiy div tag. In fact, only appears
      # at the image "title" attribute. That's the reason why this method is
      # here.
      def forum_name(element)
        element.xpath('descendant::img/@title').first.value
      end

      def username(element)
        element.xpath('descendant::div[contains(@class, "user")]/a/text()').text
      end

      def date(element)
        element.xpath('descendant::time/@datetime').first.value
      end

      def title(element)
        element.xpath('descendant::img/following-sibling::a/text()').text
      end

      def url(element)
        element.xpath('descendant::img/following-sibling::a/@href').first.value
      end
    end
  end

  # Simply store Forum data.
  #
  # Forum is not divided in the HTML. Thus, no @element to store a
  # Nokogiri::Element instance is defined in this class. This means that a
  # ForumParser just stores ActivityParser instances and call their methods
  # accordingly. Also, creates PEDCo::SectionActivity instances with the proper
  # data.
  class ForumParser
    # @param activities Nokogiri::Element
    def initialize(activities, name = nil)
      @name = if name.nil?
                parse_name
              else
                name
              end
      @activities = parse_activities activities
    end

    # The name of the forum
    attr_reader :name

    # ActivitieParser instances from the HTMl elements.
    attr_reader :activities

    def to_section_activity
      activities = @activities.map(&:to_activity)
      PEDCo::SectionActivity.new @name, activities
    end

    protected

    def parse_activities(elements)
      elements.map do |element|
        ActivityParser.new element
      end
    end

    # Return the name of the forum found on the firts activity.
    #
    # @return A String.
    def parse_name
      return '' if @activities.empty?

      ActivityParser.forum_name @activities.first
    end

    class << self
      # Find all messages of a particular Forum.
      def find_in_page(_name, _page)
        raise 'Sorry! Not implemented yet!'
      end

      def find_in_messages(name, messages)
        elements = messages.select do |msg|
          ActivityParser.forum_name(msg) == name
        end
        elements.flatten!
        ForumParser.new elements, name
      end
    end
  end

  # Parse all data from a "default" sorted activities page.
  #
  # Given a "recent activity" page, parse all relevant information from it.
  class DefaultSortParser
    def initialize(_page)
      raise 'Sorry! Not implemented yet!'
      # @elements = page.xpath '//div[contains(@class, "generalbox")]'
    end

    def to_s
      '#<PEDCoParsers::DefaultSortParser page=...>'
    end

    protected

    def parse_forums() end
  end

  # Parse all data from a "desc" sorted activities page.
  #
  # Given a "recent activity" page, parse all relevant information from it.
  #
  # In this case, the HTML page has all messages are at the same level: they are
  # not divided by forums.
  class DescSortParser
    def initialize(page)
      @page = page
      @forum_messages = DescSortParser.forum_messages page
      @forums = DescSortParser.parse_forums_from_messages @forum_messages
    end

    # The Nokogiri::Element page.
    attr_reader :page

    # A list of forums parsed from the page.
    attr_reader :forums

    attr_reader :forum_messages

    def to_recent_activity
      sections = @forums.map(&:to_section_activity)
      PEDCo::RecentActivity.new sections
    end

    def to_s
      '#<PEDCoParsers::DefaultSortParser page=...>'
    end

    # Avoid displaying all the page data which will crash any human eyes...
    def inspect
      '#<PEDCoParsers::DefaultSortParser page=...>'
    end

    class << self
      def from_file(path)
        DescSortParser.new Nokogiri::HTML File.open path
      end

      # Retrieve all Nokogiri::Element instances that contain a message.
      #
      # Apply an XPath to find HTML elements with a massage, filtering out other
      # non-related HTML elements.
      def forum_messages(page)
        res = page.xpath '//td[contains(@class, "discussion")]'
        if res.nil?
          []
        else
          res
        end
      end

      def forum_names(page)
        forum_names_from_messages forum_messages(page)
      end

      # Return all forum names from a list of messages.
      #
      # @param messages An array of Nokogiri::Element instances. Use
      # DesSortParser::forum_messages to retrieve it from a Nokogiri::Page.
      #
      # @return A list of strings.
      def forum_names_from_messages(messages)
        names = messages.map do |msg|
          ActivityParser.forum_name msg
        end
        names.uniq
      end

      # Return ForumParser instances from a Nokogiri::Page
      def parse_forums(_page)
        raise 'Sorry! Not implemented yet!'
        # @forum = forum_names(page).map do |name|
        #   ForumParser.find_in_page name, page
        # end
      end

      # Return ForumParser instances from a list ofmessages
      #
      # @param messages An array of Nokogiri::Element instances. Use
      # DesSortParser::forum_messages to retrieve it from a Nokogiri::Page.
      def parse_forums_from_messages(messages)
        forum_names_from_messages(messages).map do |name|
          ForumParser.find_in_messages name, messages
        end
      end
    end
  end

  # Parser for the user profile
  class UserParser
    def initialize(element)
      @element = element
      @my_courses = parse_my_courses
    end

    attr_reader :my_courses

    # @return Array of PEDCo::Course
    def to_courses
      @my_courses.map do |coursedata|
        PEDCo::Course.new coursedata[:course_id], coursedata[:name]
      end
    end

    protected

    def parse_my_courses
      links = UserParser.course_anchors @element
      links.map do |link|
        {
          course_id: link.get_attribute('href').match(/&course=(.*)&/)[1],
          name: link.text
        }
      end
    end

    class << self
      # Retornar todas las etiquetas A de la sección "Detalles del curso"
      #
      # @param element Nokogiri::XML::ELement
      # @return Array of Nokogiri::XML::Element
      def course_anchors(element)
        element.xpath('descendant::li/a[contains(@href,"course=")]')
      end
    end
  end

  # Parser for the Main Page
  #
  # Main page is at https://pedco.uncoma.edu.ar/my/
  class MainPageParser
    def initialize(element)
      @element = element
      @user_id = MainPageParser.user_id @element
      @profile_url = MainPageParser.profile_url @element
    end

    attr_reader :user_id, :profile_url

    class << self
      # Look for the user ID.
      #
      # @return String
      def user_id(element)
        attrs = element.xpath('descendant::div/@data-userid')
        attrs = element.xpath('descendant::div/@data-user-id') if attrs.empty?
        attrs.first.value
      end

      # Look for the user profile URL
      #
      # @return String
      def profile_url(element)
        hrefs = element.xpath 'descendant::a[@data-title="profile,moodle"]/@href'
        hrefs.first.value
      end
    end
  end

  # Forum Message Parser
  class MessageParser
    def initialize(element)
      @element = element
      @id = MessageParser.id @element
      @user = MessageParser.user @element
      @time = Time.xmlschema MessageParser.time(@element)
      @contents = MessageParser.contents @element
    end

    attr_reader :element, :id, :user, :time, :contents

    def to_message
      PEDCo::Message.new @id, @user, @time, @contents
    end

    class << self
      def id(element)
        element.xpath('@id').first.value
      end

      def user(element)
        element.xpath('descendant::a[contains(@href, "user")]').first.text
      end

      def time(element)
        element.xpath('descendant::time/@datetime').first.value
      end

      # @return Nokogiri::XML::Element
      def contents(element)
        element.xpath(
          'descendant::div[contains(@class,"post-content-container")]'
        ).first
      end
    end
  end

  # Parse forum messages
  class DiscussionForumParser
    def initialize(element)
      @element = element
      @messages = DiscussionForumParser.messages @element
      @name = DiscussionForumParser.name @element
    end

    attr_reader :messages, :name

    def to_forum
      messages = @messages.map(&:to_message)
      PEDCo::Forum.new @name, messages
    end

    class << self
      def messages(element)
        elts = element.xpath 'descendant::article'
        elts.map do |msg|
          MessageParser.new msg
        end
      end

      def name(element)
        element.xpath(
          'descendant::h3[contains(@class,"discussionname")]/text()'
        ).first.text
      end
    end
  end
end
